// Code generated by Prisma (prisma@1.34.8). DO NOT EDIT.
// Please don't change this file manually but run `prisma generate` to update it.
// For more information, please read the docs: https://www.prisma.io/docs/prisma-client/

import { DocumentNode } from "graphql";
import {
  makePrismaClientClass,
  BaseClientOptions,
  Model
} from "prisma-client-lib";
import { typeDefs } from "./prisma-schema";

export type AtLeastOne<T, U = { [K in keyof T]: Pick<T, K> }> = Partial<T> &
  U[keyof U];

export type Maybe<T> = T | undefined | null;

export interface Exists {
  customer: (where?: CustomerWhereInput) => Promise<boolean>;
  user: (where?: UserWhereInput) => Promise<boolean>;
}

export interface Node {}

export type FragmentableArray<T> = Promise<Array<T>> & Fragmentable;

export interface Fragmentable {
  $fragment<T>(fragment: string | DocumentNode): Promise<T>;
}

export interface Prisma {
  $exists: Exists;
  $graphql: <T = any>(
    query: string,
    variables?: { [key: string]: any }
  ) => Promise<T>;

  /**
   * Queries
   */

  customer: (where: CustomerWhereUniqueInput) => CustomerNullablePromise;
  customers: (args?: {
    where?: CustomerWhereInput;
    orderBy?: CustomerOrderByInput;
    skip?: Int;
    after?: String;
    before?: String;
    first?: Int;
    last?: Int;
  }) => FragmentableArray<Customer>;
  customersConnection: (args?: {
    where?: CustomerWhereInput;
    orderBy?: CustomerOrderByInput;
    skip?: Int;
    after?: String;
    before?: String;
    first?: Int;
    last?: Int;
  }) => CustomerConnectionPromise;
  user: (where: UserWhereUniqueInput) => UserNullablePromise;
  users: (args?: {
    where?: UserWhereInput;
    orderBy?: UserOrderByInput;
    skip?: Int;
    after?: String;
    before?: String;
    first?: Int;
    last?: Int;
  }) => FragmentableArray<User>;
  usersConnection: (args?: {
    where?: UserWhereInput;
    orderBy?: UserOrderByInput;
    skip?: Int;
    after?: String;
    before?: String;
    first?: Int;
    last?: Int;
  }) => UserConnectionPromise;
  node: (args: { id: ID_Output }) => Node;

  /**
   * Mutations
   */

  createCustomer: (data: CustomerCreateInput) => CustomerPromise;
  updateCustomer: (args: {
    data: CustomerUpdateInput;
    where: CustomerWhereUniqueInput;
  }) => CustomerPromise;
  updateManyCustomers: (args: {
    data: CustomerUpdateManyMutationInput;
    where?: CustomerWhereInput;
  }) => BatchPayloadPromise;
  upsertCustomer: (args: {
    where: CustomerWhereUniqueInput;
    create: CustomerCreateInput;
    update: CustomerUpdateInput;
  }) => CustomerPromise;
  deleteCustomer: (where: CustomerWhereUniqueInput) => CustomerPromise;
  deleteManyCustomers: (where?: CustomerWhereInput) => BatchPayloadPromise;
  createUser: (data: UserCreateInput) => UserPromise;
  updateUser: (args: {
    data: UserUpdateInput;
    where: UserWhereUniqueInput;
  }) => UserPromise;
  updateManyUsers: (args: {
    data: UserUpdateManyMutationInput;
    where?: UserWhereInput;
  }) => BatchPayloadPromise;
  upsertUser: (args: {
    where: UserWhereUniqueInput;
    create: UserCreateInput;
    update: UserUpdateInput;
  }) => UserPromise;
  deleteUser: (where: UserWhereUniqueInput) => UserPromise;
  deleteManyUsers: (where?: UserWhereInput) => BatchPayloadPromise;

  /**
   * Subscriptions
   */

  $subscribe: Subscription;
}

export interface Subscription {
  customer: (
    where?: CustomerSubscriptionWhereInput
  ) => CustomerSubscriptionPayloadSubscription;
  user: (
    where?: UserSubscriptionWhereInput
  ) => UserSubscriptionPayloadSubscription;
}

export interface ClientConstructor<T> {
  new (options?: BaseClientOptions): T;
}

/**
 * Types
 */

export type Disposition =
  | "ACTIVE"
  | "EXPIRED"
  | "PENDING1"
  | "PENDING2"
  | "PENDING3"
  | "RENEWED"
  | "DECLINED"
  | "LOSTCONTACT";

export type CustomerOrderByInput =
  | "id_ASC"
  | "id_DESC"
  | "fullname_ASC"
  | "fullname_DESC"
  | "prefix_ASC"
  | "prefix_DESC"
  | "policynumber_ASC"
  | "policynumber_DESC"
  | "effectivedate_ASC"
  | "effectivedate_DESC"
  | "phone1_ASC"
  | "phone1_DESC"
  | "phone2_ASC"
  | "phone2_DESC"
  | "phone3_ASC"
  | "phone3_DESC"
  | "email_ASC"
  | "email_DESC"
  | "emailauth_ASC"
  | "emailauth_DESC"
  | "smsauth_ASC"
  | "smsauth_DESC"
  | "waauth_ASC"
  | "waauth_DESC"
  | "invoicenumber_ASC"
  | "invoicenumber_DESC"
  | "premium_ASC"
  | "premium_DESC"
  | "ssastatus_ASC"
  | "ssastatus_DESC"
  | "ssaimport_ASC"
  | "ssaimport_DESC"
  | "ssacampaignid_ASC"
  | "ssacampaignid_DESC"
  | "ssaowner_ASC"
  | "ssaowner_DESC"
  | "notes_ASC"
  | "notes_DESC"
  | "disposition_ASC"
  | "disposition_DESC";

export type UserOrderByInput =
  | "id_ASC"
  | "id_DESC"
  | "email_ASC"
  | "email_DESC"
  | "password_ASC"
  | "password_DESC"
  | "name_ASC"
  | "name_DESC"
  | "token_ASC"
  | "token_DESC";

export type MutationType = "CREATED" | "UPDATED" | "DELETED";

export type CustomerWhereUniqueInput = AtLeastOne<{
  id: Maybe<ID_Input>;
}>;

export interface CustomerWhereInput {
  id?: Maybe<ID_Input>;
  id_not?: Maybe<ID_Input>;
  id_in?: Maybe<ID_Input[] | ID_Input>;
  id_not_in?: Maybe<ID_Input[] | ID_Input>;
  id_lt?: Maybe<ID_Input>;
  id_lte?: Maybe<ID_Input>;
  id_gt?: Maybe<ID_Input>;
  id_gte?: Maybe<ID_Input>;
  id_contains?: Maybe<ID_Input>;
  id_not_contains?: Maybe<ID_Input>;
  id_starts_with?: Maybe<ID_Input>;
  id_not_starts_with?: Maybe<ID_Input>;
  id_ends_with?: Maybe<ID_Input>;
  id_not_ends_with?: Maybe<ID_Input>;
  fullname?: Maybe<String>;
  fullname_not?: Maybe<String>;
  fullname_in?: Maybe<String[] | String>;
  fullname_not_in?: Maybe<String[] | String>;
  fullname_lt?: Maybe<String>;
  fullname_lte?: Maybe<String>;
  fullname_gt?: Maybe<String>;
  fullname_gte?: Maybe<String>;
  fullname_contains?: Maybe<String>;
  fullname_not_contains?: Maybe<String>;
  fullname_starts_with?: Maybe<String>;
  fullname_not_starts_with?: Maybe<String>;
  fullname_ends_with?: Maybe<String>;
  fullname_not_ends_with?: Maybe<String>;
  prefix?: Maybe<String>;
  prefix_not?: Maybe<String>;
  prefix_in?: Maybe<String[] | String>;
  prefix_not_in?: Maybe<String[] | String>;
  prefix_lt?: Maybe<String>;
  prefix_lte?: Maybe<String>;
  prefix_gt?: Maybe<String>;
  prefix_gte?: Maybe<String>;
  prefix_contains?: Maybe<String>;
  prefix_not_contains?: Maybe<String>;
  prefix_starts_with?: Maybe<String>;
  prefix_not_starts_with?: Maybe<String>;
  prefix_ends_with?: Maybe<String>;
  prefix_not_ends_with?: Maybe<String>;
  policynumber?: Maybe<String>;
  policynumber_not?: Maybe<String>;
  policynumber_in?: Maybe<String[] | String>;
  policynumber_not_in?: Maybe<String[] | String>;
  policynumber_lt?: Maybe<String>;
  policynumber_lte?: Maybe<String>;
  policynumber_gt?: Maybe<String>;
  policynumber_gte?: Maybe<String>;
  policynumber_contains?: Maybe<String>;
  policynumber_not_contains?: Maybe<String>;
  policynumber_starts_with?: Maybe<String>;
  policynumber_not_starts_with?: Maybe<String>;
  policynumber_ends_with?: Maybe<String>;
  policynumber_not_ends_with?: Maybe<String>;
  effectivedate?: Maybe<String>;
  effectivedate_not?: Maybe<String>;
  effectivedate_in?: Maybe<String[] | String>;
  effectivedate_not_in?: Maybe<String[] | String>;
  effectivedate_lt?: Maybe<String>;
  effectivedate_lte?: Maybe<String>;
  effectivedate_gt?: Maybe<String>;
  effectivedate_gte?: Maybe<String>;
  effectivedate_contains?: Maybe<String>;
  effectivedate_not_contains?: Maybe<String>;
  effectivedate_starts_with?: Maybe<String>;
  effectivedate_not_starts_with?: Maybe<String>;
  effectivedate_ends_with?: Maybe<String>;
  effectivedate_not_ends_with?: Maybe<String>;
  phone1?: Maybe<String>;
  phone1_not?: Maybe<String>;
  phone1_in?: Maybe<String[] | String>;
  phone1_not_in?: Maybe<String[] | String>;
  phone1_lt?: Maybe<String>;
  phone1_lte?: Maybe<String>;
  phone1_gt?: Maybe<String>;
  phone1_gte?: Maybe<String>;
  phone1_contains?: Maybe<String>;
  phone1_not_contains?: Maybe<String>;
  phone1_starts_with?: Maybe<String>;
  phone1_not_starts_with?: Maybe<String>;
  phone1_ends_with?: Maybe<String>;
  phone1_not_ends_with?: Maybe<String>;
  phone2?: Maybe<String>;
  phone2_not?: Maybe<String>;
  phone2_in?: Maybe<String[] | String>;
  phone2_not_in?: Maybe<String[] | String>;
  phone2_lt?: Maybe<String>;
  phone2_lte?: Maybe<String>;
  phone2_gt?: Maybe<String>;
  phone2_gte?: Maybe<String>;
  phone2_contains?: Maybe<String>;
  phone2_not_contains?: Maybe<String>;
  phone2_starts_with?: Maybe<String>;
  phone2_not_starts_with?: Maybe<String>;
  phone2_ends_with?: Maybe<String>;
  phone2_not_ends_with?: Maybe<String>;
  phone3?: Maybe<String>;
  phone3_not?: Maybe<String>;
  phone3_in?: Maybe<String[] | String>;
  phone3_not_in?: Maybe<String[] | String>;
  phone3_lt?: Maybe<String>;
  phone3_lte?: Maybe<String>;
  phone3_gt?: Maybe<String>;
  phone3_gte?: Maybe<String>;
  phone3_contains?: Maybe<String>;
  phone3_not_contains?: Maybe<String>;
  phone3_starts_with?: Maybe<String>;
  phone3_not_starts_with?: Maybe<String>;
  phone3_ends_with?: Maybe<String>;
  phone3_not_ends_with?: Maybe<String>;
  email?: Maybe<String>;
  email_not?: Maybe<String>;
  email_in?: Maybe<String[] | String>;
  email_not_in?: Maybe<String[] | String>;
  email_lt?: Maybe<String>;
  email_lte?: Maybe<String>;
  email_gt?: Maybe<String>;
  email_gte?: Maybe<String>;
  email_contains?: Maybe<String>;
  email_not_contains?: Maybe<String>;
  email_starts_with?: Maybe<String>;
  email_not_starts_with?: Maybe<String>;
  email_ends_with?: Maybe<String>;
  email_not_ends_with?: Maybe<String>;
  emailauth?: Maybe<String>;
  emailauth_not?: Maybe<String>;
  emailauth_in?: Maybe<String[] | String>;
  emailauth_not_in?: Maybe<String[] | String>;
  emailauth_lt?: Maybe<String>;
  emailauth_lte?: Maybe<String>;
  emailauth_gt?: Maybe<String>;
  emailauth_gte?: Maybe<String>;
  emailauth_contains?: Maybe<String>;
  emailauth_not_contains?: Maybe<String>;
  emailauth_starts_with?: Maybe<String>;
  emailauth_not_starts_with?: Maybe<String>;
  emailauth_ends_with?: Maybe<String>;
  emailauth_not_ends_with?: Maybe<String>;
  smsauth?: Maybe<String>;
  smsauth_not?: Maybe<String>;
  smsauth_in?: Maybe<String[] | String>;
  smsauth_not_in?: Maybe<String[] | String>;
  smsauth_lt?: Maybe<String>;
  smsauth_lte?: Maybe<String>;
  smsauth_gt?: Maybe<String>;
  smsauth_gte?: Maybe<String>;
  smsauth_contains?: Maybe<String>;
  smsauth_not_contains?: Maybe<String>;
  smsauth_starts_with?: Maybe<String>;
  smsauth_not_starts_with?: Maybe<String>;
  smsauth_ends_with?: Maybe<String>;
  smsauth_not_ends_with?: Maybe<String>;
  waauth?: Maybe<String>;
  waauth_not?: Maybe<String>;
  waauth_in?: Maybe<String[] | String>;
  waauth_not_in?: Maybe<String[] | String>;
  waauth_lt?: Maybe<String>;
  waauth_lte?: Maybe<String>;
  waauth_gt?: Maybe<String>;
  waauth_gte?: Maybe<String>;
  waauth_contains?: Maybe<String>;
  waauth_not_contains?: Maybe<String>;
  waauth_starts_with?: Maybe<String>;
  waauth_not_starts_with?: Maybe<String>;
  waauth_ends_with?: Maybe<String>;
  waauth_not_ends_with?: Maybe<String>;
  invoicenumber?: Maybe<String>;
  invoicenumber_not?: Maybe<String>;
  invoicenumber_in?: Maybe<String[] | String>;
  invoicenumber_not_in?: Maybe<String[] | String>;
  invoicenumber_lt?: Maybe<String>;
  invoicenumber_lte?: Maybe<String>;
  invoicenumber_gt?: Maybe<String>;
  invoicenumber_gte?: Maybe<String>;
  invoicenumber_contains?: Maybe<String>;
  invoicenumber_not_contains?: Maybe<String>;
  invoicenumber_starts_with?: Maybe<String>;
  invoicenumber_not_starts_with?: Maybe<String>;
  invoicenumber_ends_with?: Maybe<String>;
  invoicenumber_not_ends_with?: Maybe<String>;
  premium?: Maybe<String>;
  premium_not?: Maybe<String>;
  premium_in?: Maybe<String[] | String>;
  premium_not_in?: Maybe<String[] | String>;
  premium_lt?: Maybe<String>;
  premium_lte?: Maybe<String>;
  premium_gt?: Maybe<String>;
  premium_gte?: Maybe<String>;
  premium_contains?: Maybe<String>;
  premium_not_contains?: Maybe<String>;
  premium_starts_with?: Maybe<String>;
  premium_not_starts_with?: Maybe<String>;
  premium_ends_with?: Maybe<String>;
  premium_not_ends_with?: Maybe<String>;
  ssastatus?: Maybe<String>;
  ssastatus_not?: Maybe<String>;
  ssastatus_in?: Maybe<String[] | String>;
  ssastatus_not_in?: Maybe<String[] | String>;
  ssastatus_lt?: Maybe<String>;
  ssastatus_lte?: Maybe<String>;
  ssastatus_gt?: Maybe<String>;
  ssastatus_gte?: Maybe<String>;
  ssastatus_contains?: Maybe<String>;
  ssastatus_not_contains?: Maybe<String>;
  ssastatus_starts_with?: Maybe<String>;
  ssastatus_not_starts_with?: Maybe<String>;
  ssastatus_ends_with?: Maybe<String>;
  ssastatus_not_ends_with?: Maybe<String>;
  ssaimport?: Maybe<String>;
  ssaimport_not?: Maybe<String>;
  ssaimport_in?: Maybe<String[] | String>;
  ssaimport_not_in?: Maybe<String[] | String>;
  ssaimport_lt?: Maybe<String>;
  ssaimport_lte?: Maybe<String>;
  ssaimport_gt?: Maybe<String>;
  ssaimport_gte?: Maybe<String>;
  ssaimport_contains?: Maybe<String>;
  ssaimport_not_contains?: Maybe<String>;
  ssaimport_starts_with?: Maybe<String>;
  ssaimport_not_starts_with?: Maybe<String>;
  ssaimport_ends_with?: Maybe<String>;
  ssaimport_not_ends_with?: Maybe<String>;
  ssacampaignid?: Maybe<String>;
  ssacampaignid_not?: Maybe<String>;
  ssacampaignid_in?: Maybe<String[] | String>;
  ssacampaignid_not_in?: Maybe<String[] | String>;
  ssacampaignid_lt?: Maybe<String>;
  ssacampaignid_lte?: Maybe<String>;
  ssacampaignid_gt?: Maybe<String>;
  ssacampaignid_gte?: Maybe<String>;
  ssacampaignid_contains?: Maybe<String>;
  ssacampaignid_not_contains?: Maybe<String>;
  ssacampaignid_starts_with?: Maybe<String>;
  ssacampaignid_not_starts_with?: Maybe<String>;
  ssacampaignid_ends_with?: Maybe<String>;
  ssacampaignid_not_ends_with?: Maybe<String>;
  ssaowner?: Maybe<String>;
  ssaowner_not?: Maybe<String>;
  ssaowner_in?: Maybe<String[] | String>;
  ssaowner_not_in?: Maybe<String[] | String>;
  ssaowner_lt?: Maybe<String>;
  ssaowner_lte?: Maybe<String>;
  ssaowner_gt?: Maybe<String>;
  ssaowner_gte?: Maybe<String>;
  ssaowner_contains?: Maybe<String>;
  ssaowner_not_contains?: Maybe<String>;
  ssaowner_starts_with?: Maybe<String>;
  ssaowner_not_starts_with?: Maybe<String>;
  ssaowner_ends_with?: Maybe<String>;
  ssaowner_not_ends_with?: Maybe<String>;
  notes?: Maybe<String>;
  notes_not?: Maybe<String>;
  notes_in?: Maybe<String[] | String>;
  notes_not_in?: Maybe<String[] | String>;
  notes_lt?: Maybe<String>;
  notes_lte?: Maybe<String>;
  notes_gt?: Maybe<String>;
  notes_gte?: Maybe<String>;
  notes_contains?: Maybe<String>;
  notes_not_contains?: Maybe<String>;
  notes_starts_with?: Maybe<String>;
  notes_not_starts_with?: Maybe<String>;
  notes_ends_with?: Maybe<String>;
  notes_not_ends_with?: Maybe<String>;
  disposition?: Maybe<Disposition>;
  disposition_not?: Maybe<Disposition>;
  disposition_in?: Maybe<Disposition[] | Disposition>;
  disposition_not_in?: Maybe<Disposition[] | Disposition>;
  AND?: Maybe<CustomerWhereInput[] | CustomerWhereInput>;
  OR?: Maybe<CustomerWhereInput[] | CustomerWhereInput>;
  NOT?: Maybe<CustomerWhereInput[] | CustomerWhereInput>;
}

export type UserWhereUniqueInput = AtLeastOne<{
  id: Maybe<ID_Input>;
  email?: Maybe<String>;
}>;

export interface UserWhereInput {
  id?: Maybe<ID_Input>;
  id_not?: Maybe<ID_Input>;
  id_in?: Maybe<ID_Input[] | ID_Input>;
  id_not_in?: Maybe<ID_Input[] | ID_Input>;
  id_lt?: Maybe<ID_Input>;
  id_lte?: Maybe<ID_Input>;
  id_gt?: Maybe<ID_Input>;
  id_gte?: Maybe<ID_Input>;
  id_contains?: Maybe<ID_Input>;
  id_not_contains?: Maybe<ID_Input>;
  id_starts_with?: Maybe<ID_Input>;
  id_not_starts_with?: Maybe<ID_Input>;
  id_ends_with?: Maybe<ID_Input>;
  id_not_ends_with?: Maybe<ID_Input>;
  email?: Maybe<String>;
  email_not?: Maybe<String>;
  email_in?: Maybe<String[] | String>;
  email_not_in?: Maybe<String[] | String>;
  email_lt?: Maybe<String>;
  email_lte?: Maybe<String>;
  email_gt?: Maybe<String>;
  email_gte?: Maybe<String>;
  email_contains?: Maybe<String>;
  email_not_contains?: Maybe<String>;
  email_starts_with?: Maybe<String>;
  email_not_starts_with?: Maybe<String>;
  email_ends_with?: Maybe<String>;
  email_not_ends_with?: Maybe<String>;
  password?: Maybe<String>;
  password_not?: Maybe<String>;
  password_in?: Maybe<String[] | String>;
  password_not_in?: Maybe<String[] | String>;
  password_lt?: Maybe<String>;
  password_lte?: Maybe<String>;
  password_gt?: Maybe<String>;
  password_gte?: Maybe<String>;
  password_contains?: Maybe<String>;
  password_not_contains?: Maybe<String>;
  password_starts_with?: Maybe<String>;
  password_not_starts_with?: Maybe<String>;
  password_ends_with?: Maybe<String>;
  password_not_ends_with?: Maybe<String>;
  name?: Maybe<String>;
  name_not?: Maybe<String>;
  name_in?: Maybe<String[] | String>;
  name_not_in?: Maybe<String[] | String>;
  name_lt?: Maybe<String>;
  name_lte?: Maybe<String>;
  name_gt?: Maybe<String>;
  name_gte?: Maybe<String>;
  name_contains?: Maybe<String>;
  name_not_contains?: Maybe<String>;
  name_starts_with?: Maybe<String>;
  name_not_starts_with?: Maybe<String>;
  name_ends_with?: Maybe<String>;
  name_not_ends_with?: Maybe<String>;
  token?: Maybe<String>;
  token_not?: Maybe<String>;
  token_in?: Maybe<String[] | String>;
  token_not_in?: Maybe<String[] | String>;
  token_lt?: Maybe<String>;
  token_lte?: Maybe<String>;
  token_gt?: Maybe<String>;
  token_gte?: Maybe<String>;
  token_contains?: Maybe<String>;
  token_not_contains?: Maybe<String>;
  token_starts_with?: Maybe<String>;
  token_not_starts_with?: Maybe<String>;
  token_ends_with?: Maybe<String>;
  token_not_ends_with?: Maybe<String>;
  AND?: Maybe<UserWhereInput[] | UserWhereInput>;
  OR?: Maybe<UserWhereInput[] | UserWhereInput>;
  NOT?: Maybe<UserWhereInput[] | UserWhereInput>;
}

export interface CustomerCreateInput {
  id?: Maybe<ID_Input>;
  fullname: String;
  prefix?: Maybe<String>;
  policynumber: String;
  effectivedate: String;
  phone1: String;
  phone2?: Maybe<String>;
  phone3?: Maybe<String>;
  email?: Maybe<String>;
  emailauth?: Maybe<String>;
  smsauth?: Maybe<String>;
  waauth?: Maybe<String>;
  invoicenumber: String;
  premium: String;
  ssastatus?: Maybe<String>;
  ssaimport?: Maybe<String>;
  ssacampaignid: String;
  ssaowner?: Maybe<String>;
  notes?: Maybe<String>;
  disposition: Disposition;
}

export interface CustomerUpdateInput {
  fullname?: Maybe<String>;
  prefix?: Maybe<String>;
  policynumber?: Maybe<String>;
  effectivedate?: Maybe<String>;
  phone1?: Maybe<String>;
  phone2?: Maybe<String>;
  phone3?: Maybe<String>;
  email?: Maybe<String>;
  emailauth?: Maybe<String>;
  smsauth?: Maybe<String>;
  waauth?: Maybe<String>;
  invoicenumber?: Maybe<String>;
  premium?: Maybe<String>;
  ssastatus?: Maybe<String>;
  ssaimport?: Maybe<String>;
  ssacampaignid?: Maybe<String>;
  ssaowner?: Maybe<String>;
  notes?: Maybe<String>;
  disposition?: Maybe<Disposition>;
}

export interface CustomerUpdateManyMutationInput {
  fullname?: Maybe<String>;
  prefix?: Maybe<String>;
  policynumber?: Maybe<String>;
  effectivedate?: Maybe<String>;
  phone1?: Maybe<String>;
  phone2?: Maybe<String>;
  phone3?: Maybe<String>;
  email?: Maybe<String>;
  emailauth?: Maybe<String>;
  smsauth?: Maybe<String>;
  waauth?: Maybe<String>;
  invoicenumber?: Maybe<String>;
  premium?: Maybe<String>;
  ssastatus?: Maybe<String>;
  ssaimport?: Maybe<String>;
  ssacampaignid?: Maybe<String>;
  ssaowner?: Maybe<String>;
  notes?: Maybe<String>;
  disposition?: Maybe<Disposition>;
}

export interface UserCreateInput {
  id?: Maybe<ID_Input>;
  email: String;
  password: String;
  name: String;
  token?: Maybe<String>;
}

export interface UserUpdateInput {
  email?: Maybe<String>;
  password?: Maybe<String>;
  name?: Maybe<String>;
  token?: Maybe<String>;
}

export interface UserUpdateManyMutationInput {
  email?: Maybe<String>;
  password?: Maybe<String>;
  name?: Maybe<String>;
  token?: Maybe<String>;
}

export interface CustomerSubscriptionWhereInput {
  mutation_in?: Maybe<MutationType[] | MutationType>;
  updatedFields_contains?: Maybe<String>;
  updatedFields_contains_every?: Maybe<String[] | String>;
  updatedFields_contains_some?: Maybe<String[] | String>;
  node?: Maybe<CustomerWhereInput>;
  AND?: Maybe<
    CustomerSubscriptionWhereInput[] | CustomerSubscriptionWhereInput
  >;
  OR?: Maybe<CustomerSubscriptionWhereInput[] | CustomerSubscriptionWhereInput>;
  NOT?: Maybe<
    CustomerSubscriptionWhereInput[] | CustomerSubscriptionWhereInput
  >;
}

export interface UserSubscriptionWhereInput {
  mutation_in?: Maybe<MutationType[] | MutationType>;
  updatedFields_contains?: Maybe<String>;
  updatedFields_contains_every?: Maybe<String[] | String>;
  updatedFields_contains_some?: Maybe<String[] | String>;
  node?: Maybe<UserWhereInput>;
  AND?: Maybe<UserSubscriptionWhereInput[] | UserSubscriptionWhereInput>;
  OR?: Maybe<UserSubscriptionWhereInput[] | UserSubscriptionWhereInput>;
  NOT?: Maybe<UserSubscriptionWhereInput[] | UserSubscriptionWhereInput>;
}

export interface NodeNode {
  id: ID_Output;
}

export interface Customer {
  id: ID_Output;
  fullname: String;
  prefix?: String;
  policynumber: String;
  effectivedate: String;
  phone1: String;
  phone2?: String;
  phone3?: String;
  email?: String;
  emailauth?: String;
  smsauth?: String;
  waauth?: String;
  invoicenumber: String;
  premium: String;
  ssastatus?: String;
  ssaimport?: String;
  ssacampaignid: String;
  ssaowner?: String;
  notes?: String;
  disposition: Disposition;
}

export interface CustomerPromise extends Promise<Customer>, Fragmentable {
  id: () => Promise<ID_Output>;
  fullname: () => Promise<String>;
  prefix: () => Promise<String>;
  policynumber: () => Promise<String>;
  effectivedate: () => Promise<String>;
  phone1: () => Promise<String>;
  phone2: () => Promise<String>;
  phone3: () => Promise<String>;
  email: () => Promise<String>;
  emailauth: () => Promise<String>;
  smsauth: () => Promise<String>;
  waauth: () => Promise<String>;
  invoicenumber: () => Promise<String>;
  premium: () => Promise<String>;
  ssastatus: () => Promise<String>;
  ssaimport: () => Promise<String>;
  ssacampaignid: () => Promise<String>;
  ssaowner: () => Promise<String>;
  notes: () => Promise<String>;
  disposition: () => Promise<Disposition>;
}

export interface CustomerSubscription
  extends Promise<AsyncIterator<Customer>>,
    Fragmentable {
  id: () => Promise<AsyncIterator<ID_Output>>;
  fullname: () => Promise<AsyncIterator<String>>;
  prefix: () => Promise<AsyncIterator<String>>;
  policynumber: () => Promise<AsyncIterator<String>>;
  effectivedate: () => Promise<AsyncIterator<String>>;
  phone1: () => Promise<AsyncIterator<String>>;
  phone2: () => Promise<AsyncIterator<String>>;
  phone3: () => Promise<AsyncIterator<String>>;
  email: () => Promise<AsyncIterator<String>>;
  emailauth: () => Promise<AsyncIterator<String>>;
  smsauth: () => Promise<AsyncIterator<String>>;
  waauth: () => Promise<AsyncIterator<String>>;
  invoicenumber: () => Promise<AsyncIterator<String>>;
  premium: () => Promise<AsyncIterator<String>>;
  ssastatus: () => Promise<AsyncIterator<String>>;
  ssaimport: () => Promise<AsyncIterator<String>>;
  ssacampaignid: () => Promise<AsyncIterator<String>>;
  ssaowner: () => Promise<AsyncIterator<String>>;
  notes: () => Promise<AsyncIterator<String>>;
  disposition: () => Promise<AsyncIterator<Disposition>>;
}

export interface CustomerNullablePromise
  extends Promise<Customer | null>,
    Fragmentable {
  id: () => Promise<ID_Output>;
  fullname: () => Promise<String>;
  prefix: () => Promise<String>;
  policynumber: () => Promise<String>;
  effectivedate: () => Promise<String>;
  phone1: () => Promise<String>;
  phone2: () => Promise<String>;
  phone3: () => Promise<String>;
  email: () => Promise<String>;
  emailauth: () => Promise<String>;
  smsauth: () => Promise<String>;
  waauth: () => Promise<String>;
  invoicenumber: () => Promise<String>;
  premium: () => Promise<String>;
  ssastatus: () => Promise<String>;
  ssaimport: () => Promise<String>;
  ssacampaignid: () => Promise<String>;
  ssaowner: () => Promise<String>;
  notes: () => Promise<String>;
  disposition: () => Promise<Disposition>;
}

export interface CustomerConnection {
  pageInfo: PageInfo;
  edges: CustomerEdge[];
}

export interface CustomerConnectionPromise
  extends Promise<CustomerConnection>,
    Fragmentable {
  pageInfo: <T = PageInfoPromise>() => T;
  edges: <T = FragmentableArray<CustomerEdge>>() => T;
  aggregate: <T = AggregateCustomerPromise>() => T;
}

export interface CustomerConnectionSubscription
  extends Promise<AsyncIterator<CustomerConnection>>,
    Fragmentable {
  pageInfo: <T = PageInfoSubscription>() => T;
  edges: <T = Promise<AsyncIterator<CustomerEdgeSubscription>>>() => T;
  aggregate: <T = AggregateCustomerSubscription>() => T;
}

export interface PageInfo {
  hasNextPage: Boolean;
  hasPreviousPage: Boolean;
  startCursor?: String;
  endCursor?: String;
}

export interface PageInfoPromise extends Promise<PageInfo>, Fragmentable {
  hasNextPage: () => Promise<Boolean>;
  hasPreviousPage: () => Promise<Boolean>;
  startCursor: () => Promise<String>;
  endCursor: () => Promise<String>;
}

export interface PageInfoSubscription
  extends Promise<AsyncIterator<PageInfo>>,
    Fragmentable {
  hasNextPage: () => Promise<AsyncIterator<Boolean>>;
  hasPreviousPage: () => Promise<AsyncIterator<Boolean>>;
  startCursor: () => Promise<AsyncIterator<String>>;
  endCursor: () => Promise<AsyncIterator<String>>;
}

export interface CustomerEdge {
  node: Customer;
  cursor: String;
}

export interface CustomerEdgePromise
  extends Promise<CustomerEdge>,
    Fragmentable {
  node: <T = CustomerPromise>() => T;
  cursor: () => Promise<String>;
}

export interface CustomerEdgeSubscription
  extends Promise<AsyncIterator<CustomerEdge>>,
    Fragmentable {
  node: <T = CustomerSubscription>() => T;
  cursor: () => Promise<AsyncIterator<String>>;
}

export interface AggregateCustomer {
  count: Int;
}

export interface AggregateCustomerPromise
  extends Promise<AggregateCustomer>,
    Fragmentable {
  count: () => Promise<Int>;
}

export interface AggregateCustomerSubscription
  extends Promise<AsyncIterator<AggregateCustomer>>,
    Fragmentable {
  count: () => Promise<AsyncIterator<Int>>;
}

export interface User {
  id: ID_Output;
  email: String;
  password: String;
  name: String;
  token?: String;
}

export interface UserPromise extends Promise<User>, Fragmentable {
  id: () => Promise<ID_Output>;
  email: () => Promise<String>;
  password: () => Promise<String>;
  name: () => Promise<String>;
  token: () => Promise<String>;
}

export interface UserSubscription
  extends Promise<AsyncIterator<User>>,
    Fragmentable {
  id: () => Promise<AsyncIterator<ID_Output>>;
  email: () => Promise<AsyncIterator<String>>;
  password: () => Promise<AsyncIterator<String>>;
  name: () => Promise<AsyncIterator<String>>;
  token: () => Promise<AsyncIterator<String>>;
}

export interface UserNullablePromise
  extends Promise<User | null>,
    Fragmentable {
  id: () => Promise<ID_Output>;
  email: () => Promise<String>;
  password: () => Promise<String>;
  name: () => Promise<String>;
  token: () => Promise<String>;
}

export interface UserConnection {
  pageInfo: PageInfo;
  edges: UserEdge[];
}

export interface UserConnectionPromise
  extends Promise<UserConnection>,
    Fragmentable {
  pageInfo: <T = PageInfoPromise>() => T;
  edges: <T = FragmentableArray<UserEdge>>() => T;
  aggregate: <T = AggregateUserPromise>() => T;
}

export interface UserConnectionSubscription
  extends Promise<AsyncIterator<UserConnection>>,
    Fragmentable {
  pageInfo: <T = PageInfoSubscription>() => T;
  edges: <T = Promise<AsyncIterator<UserEdgeSubscription>>>() => T;
  aggregate: <T = AggregateUserSubscription>() => T;
}

export interface UserEdge {
  node: User;
  cursor: String;
}

export interface UserEdgePromise extends Promise<UserEdge>, Fragmentable {
  node: <T = UserPromise>() => T;
  cursor: () => Promise<String>;
}

export interface UserEdgeSubscription
  extends Promise<AsyncIterator<UserEdge>>,
    Fragmentable {
  node: <T = UserSubscription>() => T;
  cursor: () => Promise<AsyncIterator<String>>;
}

export interface AggregateUser {
  count: Int;
}

export interface AggregateUserPromise
  extends Promise<AggregateUser>,
    Fragmentable {
  count: () => Promise<Int>;
}

export interface AggregateUserSubscription
  extends Promise<AsyncIterator<AggregateUser>>,
    Fragmentable {
  count: () => Promise<AsyncIterator<Int>>;
}

export interface BatchPayload {
  count: Long;
}

export interface BatchPayloadPromise
  extends Promise<BatchPayload>,
    Fragmentable {
  count: () => Promise<Long>;
}

export interface BatchPayloadSubscription
  extends Promise<AsyncIterator<BatchPayload>>,
    Fragmentable {
  count: () => Promise<AsyncIterator<Long>>;
}

export interface CustomerSubscriptionPayload {
  mutation: MutationType;
  node: Customer;
  updatedFields: String[];
  previousValues: CustomerPreviousValues;
}

export interface CustomerSubscriptionPayloadPromise
  extends Promise<CustomerSubscriptionPayload>,
    Fragmentable {
  mutation: () => Promise<MutationType>;
  node: <T = CustomerPromise>() => T;
  updatedFields: () => Promise<String[]>;
  previousValues: <T = CustomerPreviousValuesPromise>() => T;
}

export interface CustomerSubscriptionPayloadSubscription
  extends Promise<AsyncIterator<CustomerSubscriptionPayload>>,
    Fragmentable {
  mutation: () => Promise<AsyncIterator<MutationType>>;
  node: <T = CustomerSubscription>() => T;
  updatedFields: () => Promise<AsyncIterator<String[]>>;
  previousValues: <T = CustomerPreviousValuesSubscription>() => T;
}

export interface CustomerPreviousValues {
  id: ID_Output;
  fullname: String;
  prefix?: String;
  policynumber: String;
  effectivedate: String;
  phone1: String;
  phone2?: String;
  phone3?: String;
  email?: String;
  emailauth?: String;
  smsauth?: String;
  waauth?: String;
  invoicenumber: String;
  premium: String;
  ssastatus?: String;
  ssaimport?: String;
  ssacampaignid: String;
  ssaowner?: String;
  notes?: String;
  disposition: Disposition;
}

export interface CustomerPreviousValuesPromise
  extends Promise<CustomerPreviousValues>,
    Fragmentable {
  id: () => Promise<ID_Output>;
  fullname: () => Promise<String>;
  prefix: () => Promise<String>;
  policynumber: () => Promise<String>;
  effectivedate: () => Promise<String>;
  phone1: () => Promise<String>;
  phone2: () => Promise<String>;
  phone3: () => Promise<String>;
  email: () => Promise<String>;
  emailauth: () => Promise<String>;
  smsauth: () => Promise<String>;
  waauth: () => Promise<String>;
  invoicenumber: () => Promise<String>;
  premium: () => Promise<String>;
  ssastatus: () => Promise<String>;
  ssaimport: () => Promise<String>;
  ssacampaignid: () => Promise<String>;
  ssaowner: () => Promise<String>;
  notes: () => Promise<String>;
  disposition: () => Promise<Disposition>;
}

export interface CustomerPreviousValuesSubscription
  extends Promise<AsyncIterator<CustomerPreviousValues>>,
    Fragmentable {
  id: () => Promise<AsyncIterator<ID_Output>>;
  fullname: () => Promise<AsyncIterator<String>>;
  prefix: () => Promise<AsyncIterator<String>>;
  policynumber: () => Promise<AsyncIterator<String>>;
  effectivedate: () => Promise<AsyncIterator<String>>;
  phone1: () => Promise<AsyncIterator<String>>;
  phone2: () => Promise<AsyncIterator<String>>;
  phone3: () => Promise<AsyncIterator<String>>;
  email: () => Promise<AsyncIterator<String>>;
  emailauth: () => Promise<AsyncIterator<String>>;
  smsauth: () => Promise<AsyncIterator<String>>;
  waauth: () => Promise<AsyncIterator<String>>;
  invoicenumber: () => Promise<AsyncIterator<String>>;
  premium: () => Promise<AsyncIterator<String>>;
  ssastatus: () => Promise<AsyncIterator<String>>;
  ssaimport: () => Promise<AsyncIterator<String>>;
  ssacampaignid: () => Promise<AsyncIterator<String>>;
  ssaowner: () => Promise<AsyncIterator<String>>;
  notes: () => Promise<AsyncIterator<String>>;
  disposition: () => Promise<AsyncIterator<Disposition>>;
}

export interface UserSubscriptionPayload {
  mutation: MutationType;
  node: User;
  updatedFields: String[];
  previousValues: UserPreviousValues;
}

export interface UserSubscriptionPayloadPromise
  extends Promise<UserSubscriptionPayload>,
    Fragmentable {
  mutation: () => Promise<MutationType>;
  node: <T = UserPromise>() => T;
  updatedFields: () => Promise<String[]>;
  previousValues: <T = UserPreviousValuesPromise>() => T;
}

export interface UserSubscriptionPayloadSubscription
  extends Promise<AsyncIterator<UserSubscriptionPayload>>,
    Fragmentable {
  mutation: () => Promise<AsyncIterator<MutationType>>;
  node: <T = UserSubscription>() => T;
  updatedFields: () => Promise<AsyncIterator<String[]>>;
  previousValues: <T = UserPreviousValuesSubscription>() => T;
}

export interface UserPreviousValues {
  id: ID_Output;
  email: String;
  password: String;
  name: String;
  token?: String;
}

export interface UserPreviousValuesPromise
  extends Promise<UserPreviousValues>,
    Fragmentable {
  id: () => Promise<ID_Output>;
  email: () => Promise<String>;
  password: () => Promise<String>;
  name: () => Promise<String>;
  token: () => Promise<String>;
}

export interface UserPreviousValuesSubscription
  extends Promise<AsyncIterator<UserPreviousValues>>,
    Fragmentable {
  id: () => Promise<AsyncIterator<ID_Output>>;
  email: () => Promise<AsyncIterator<String>>;
  password: () => Promise<AsyncIterator<String>>;
  name: () => Promise<AsyncIterator<String>>;
  token: () => Promise<AsyncIterator<String>>;
}

/*
The `ID` scalar type represents a unique identifier, often used to refetch an object or as key for a cache. The ID type appears in a JSON response as a String; however, it is not intended to be human-readable. When expected as an input type, any string (such as `"4"`) or integer (such as `4`) input value will be accepted as an ID.
*/
export type ID_Input = string | number;
export type ID_Output = string;

/*
The `String` scalar type represents textual data, represented as UTF-8 character sequences. The String type is most often used by GraphQL to represent free-form human-readable text.
*/
export type String = string;

/*
The `Int` scalar type represents non-fractional signed whole numeric values. Int can represent values between -(2^31) and 2^31 - 1.
*/
export type Int = number;

/*
The `Boolean` scalar type represents `true` or `false`.
*/
export type Boolean = boolean;

export type Long = string;

/**
 * Model Metadata
 */

export const models: Model[] = [
  {
    name: "User",
    embedded: false
  },
  {
    name: "Customer",
    embedded: false
  },
  {
    name: "Disposition",
    embedded: false
  }
];

/**
 * Type Defs
 */

export const prisma: Prisma;
